---
layout: post
title: EV3 Slow Motor Loop
date: 2016-07-22 20:23:06 +0800
last_modified_at: 2021-02-25 17:06:59 +0800
tags: en lego ev3
---

I've been practicing for [WRO 2016](http://www.wro2016india.org/) recently (just got first prize in the local competition :)), thus often messing around with [EV3](http://www.lego.com/en-us/mindstorms/about-ev3). To be honest, I've never had good experiences with these "graphical programming" stuff, and EV3-G holds the record for being the most obnoxious one (well, maybe sharing the place with [AppInventor](http://appinventor.mit.edu/explore/)).

Anyway, I encountered a problem where the motor would become slow if I put a "motor on" block in a loop. This was irritating since I wanted to improve the speed of line following. Pseudocode is below:

```
while (true) {
  turn = getColor(color = white)
  motorTurn(turn = (turn - 50) * 0.5, speed = 100)
}
```

After a bit of research, I found out that normal motor blocks are "regulated", which means that the system checks the speed of the motor rotation once in a while, and adjusts the power given based on the data. However, if the system is under load, i.e. running a busy loop, the scheduler will mess up and not call the motor check as frequent as it should.

Using the "unregulated motor" block works like a charm. However, it works by passing speeds to each motor instead of a "turn" parameter like the method above. Thus, my code had to be changed as the following:

```
while (true) {
  error = getColor(color = white) * 0.5
  leftMotorUnregulated(speed = (100 + error))
  rightMotorUnregulated(speed = (100 - error))
}
```

This doesn't seem very complicated, but once you moved it to EV3-G, the cables would tangle all together, killing the readability.

