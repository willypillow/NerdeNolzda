---
layout: post
title: "URL Change"
date: 2017-01-24 07:13:00 +0800
last_modified_at: 2020-02-16 16:08:23 +0800
tags: en site
---

As decribed in [this post]({% post_url 2017-01-23-software-and-site-changes %}), I've moved away from Cloudflare, and turned back to Namecheap, where I bought the domain, for DNS managing.

However, it turned out that Namecheap does not support CNAME flattening. That is, I can't use a CNAME record on the naked domain (i.e. nerde.pw) without breaking email MX records. (See [this link](https://serverfault.com/questions/613829/why-cant-a-cname-record-be-used-at-the-apex-aka-root-of-a-domain) for more details)

As such, the URL of this site will be changed to <https://blog.nerde.pw/>. The old URL <http://nerde.pw/> will still redirect to the new page, but keep in mind that the old one does not allow HTTPS connections due to Namecheap URL forwarding not supporting it.

---

EDIT: I created a dummy Netlify site, which accepts HTTPS, at <https://nerde.pw/> that redirects to the current one. However, it is still advised that you use the new URL in case of future changes.

The perspicacious reader might see that if I could create a site at the old URL, I could put this site there in the first place. However, the DNS records at the two domains are actually quite different. For the redirector at the naked domain, I only used an A record pointing to one of Netlify's US servers. This potentially makes loading speeds slower, since it's not taking full advantage of Netlify's CDN. On the other hand, `blog.nerde.pw` uses CNAME record that points to Netlify's domain, allowing it to return different IPs based on your location.
