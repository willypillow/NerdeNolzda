---
layout: post
title: Setting Up Yubikey and Namecoin
date: 2018-08-24 09:00:00 +0800
last_modified_at: 2021-02-25 17:07:16 +0800
tags: en security gpg encryption yubikey zeronet namecoin
---

First of all, I want to apologize for forgetting to update my Namecoin domain and Zeronet mirror for such a long time.

Anyway, I will be off to another city soon, leaving my Qubes OS desktop behind. However, due to performance and battery life concerns, I probably will not be running Qubes on my (rather old) laptop. This means that I need another way to manage PGP keys and such instead of VMs.

I had been eyeing the [Yubikey](https://www.yubico.com/) (and similar hardware security keys) for a long time, so I thought I might as well try it out. I got the regular Yubikey 4 instead of the Neo because of the price and that some say that the Neo does not support 4096 bit RSA keys. (Again, some seem to say that they do, so if you want to get a Neo, you might want to do some research on this.)

## Packaging ##
It comes in [tamper-evident](https://twitter.com/peterktodd/status/987229304185589760) packaging, so you might want to inspect that before opening.

## U2F ##
[U2F](https://en.wikipedia.org/wiki/Universal_2nd_Factor) is a two-factor authenication standard that also protects against phishing.

To test the U2F (and also [OTP](https://developers.yubico.com/OTP/OTPs_Explained.html)) functionality, [the official demo site](https://demo.yubico.com/) can be used.

Setting up and logging in via U2F is pretty straightforward and supported by [a lot of major sites](https://www.dongleauth.info/). Just enable it in the website's settings, plug the key in, and touch the metallic button.

On Firefox, you may need to follow [this guide](https://www.yubico.com/2017/11/how-to-navigate-fido-u2f-in-firefox-quantum/) to enable U2F. Also, due to [some implementation incompatibilities](https://bugzilla.mozilla.org/show_bug.cgi?id=1409573), Chrome / Chromium is needed to add keys to Google, though logging in can still be done with Firefox.

## PGP / SSH ##
Before getting the Yubikey, I have no idea that PGP keys can also double as SSH authentication keys.

[There is a pretty nice guide](https://github.com/drduh/YubiKey-Guide) about setting up PGP and SSH. Note that while the [product page](https://www.yubico.com/product/yubikey-4-series/) claims that ECC is supported, it is actually referring to [PIV](https://developers.yubico.com/PIV/), and only RSA keys (<= 4096 bits) are supported in PGP.

Like the guide suggested, only my sub-keys are transferred to the Yubikey, while the encrypted master key lies on a USB drive and is accessed with [Tails](https://tails.boum.org/).

There are only three PGP key slots: signature, encryption, and authentication. However, it seems that they are not limited to the stated purpose, e.g. you can sign with the authentication key and vice versa.

## OATH ##
The Yubikey also supports [TOTP and HOTP](https://developers.yubico.com/OATH/) (i.e. the numeric codes in [Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en_us)), though since using it on my phone would be a bit inconvenient, I am still just using an app right now.

## Backing Up ##
Yubico (the company that manufactures Yubikeys) advised [having a backup device](https://www.yubico.com/2017/04/backup-recovery-plan/), but since for U2F I can still use TOTP on my phone while for PGP my master keys are already stored elsewhere, I find it unnecessary.

## End Notes ##
In conclusion, I think it is a great purchase. The security benefits of isolating confidential data on a separate device aside, the hardware is compact and sturdy, and the overall user experience is pretty nice. U2F is definitely more convenient and secure than TOTP, and using PGP is sort of like split-GPG in Qubes, but with touching a physical button instead of clicking in a dialog.

On the other hand, after my purchase[^1], there was [this scandal](https://pwnaccelerator.github.io/2018/webusb-yubico-disclosure.html) about Yubico, which kind of sucks. Also, [some say that it is not as indestructible as it claims to be](https://news.ycombinator.com/item?id=10478932), though that is about an older version, and at least [newer versions](https://forum.yubico.com/viewtopicaa6f.html?f=35&t=2730) mitigates the acetone attack. Last but not least, some might dislike that a part of it, including the PGP portion, [is closed source](https://news.ycombinator.com/item?id=11690774). These are probably things to consider if you are thinking of getting one.

[^1]: Yes, I bought it a few months ago. To be honest this article is way overdue ;-)
