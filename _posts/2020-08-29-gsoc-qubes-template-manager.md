---
layout: post
title: "Google Summer of Code 2020: Qubes Template Manager"
date: 2020-08-29 00:00:00 +0800
last_modified_at: 2021-02-28 03:14:52 +0800
tags: en qubes gsoc
---

## Table of Contents ##

- Seed list
{:toc}

## Introduction ##

As one may see in my previous [posts][qubes-tag], I am a big fan of [Qubes OS], leading me to have an inclination to work on it in the 2020 Google Summer of Code program.

My proposal, briefly speaking, was to design a new mechanism for TemplateVM distribution and a unified tool for template management.

As this article serves as the final submission for the project, it will focus on linking to the work I have done. If you are interested in the template manager itself, the [design docs](#design-document) is a good place to start with.

[qubes-tag]: {{ site.baseurl }}/tag/qubes.html

[Qubes OS]: https://www.qubes-os.org/

## Motivation ##

Previously, TemplateVMs were distributed by RPM packages and managed by `yum`/`dnf`. However, tracking inherently dynamic VM images with a package manager suited for static files creates some challenges. For example, users may accidentally update the images, overriding local changes ([#996], [#1647]). (Or in the case of [#2061], want to specifically override the changes.) Other operations that work well on normal VMs are also somewhat inconsistent on RPM-managed templates. This includes actions such as renaming ([#839]), removal ([#5509]) and backup/restore ([#1385], [#1453], [discussion thread 1], [discussion thread 2]). In turn, this creates inconveniences and confusion for users ([#1403], [#4518]). Also, the usage of RPM packages meant that installing a template results in arbitrary code execution, which is not ideal.

Besides distribution, users may also wish to have an integrated template management application ([#2062], [#2064], [#2534], [#3040]), as opposed to the situation where multiple programs are required for different purposes, e.g., `qubes-dom0-update`, `dnf`, `qvm-remove`, `qubes-manager`.

To tackle these issues, `qvm-template` is created. It strives to provide not only a better mechanism for handling template installation but also a consistent user-facing interface to deal with template management.

[#996]: https://github.com/QubesOS/qubes-issues/issues/996
[#1647]: https://github.com/QubesOS/qubes-issues/issues/1647
[#2061]: https://github.com/QubesOS/qubes-issues/issues/2061
[#839]: https://github.com/QubesOS/qubes-issues/issues/839
[#5509]: https://github.com/QubesOS/qubes-issues/issues/5509
[#1385]: https://github.com/QubesOS/qubes-issues/issues/1385
[#1453]: https://github.com/QubesOS/qubes-issues/issues/1453
[#1403]: https://github.com/QubesOS/qubes-issues/issues/1403
[#4518]: https://github.com/QubesOS/qubes-issues/issues/4518
[#2062]: https://github.com/QubesOS/qubes-issues/issues/2062
[#2064]: https://github.com/QubesOS/qubes-issues/issues/2064
[#2534]: https://github.com/QubesOS/qubes-issues/issues/2534
[#3040]: https://github.com/QubesOS/qubes-issues/issues/3040
[discussion thread 1]: https://groups.google.com/forum/#!topic/qubes-devel/rwc2_miCNNE/discussion
[discussion thread 2]: https://groups.google.com/forum/#!topic/qubes-users/uQEUpv4THsY/discussion

## GSoC Project Page ##

- <https://summerofcode.withgoogle.com/projects/#6190965460566016>

## Proposal ##

- <https://hackmd.io/aYauztkGR0iOIoh8fJLecw>

## Design Document ##

- <https://www.qubes-os.org/doc/template-manager/> (Not yet live, [PR])
- Old version: <https://gist.github.com/WillyPillow/b8a643ddbd9235a97bc187e6e44b16e4>

[PR]: https://github.com/QubesOS/qubes-doc/pull/1031

## Contributions ##

- <https://github.com/orgs/QubesOS/projects/1>: Main Github Project
- [`qubes-repo-templates`][qubes-repo-templates]: Package containing template repo configuration and keys
- New Admin API call: `admin.vm.Volume.Clear`
	- Fixes [#5946], a bug discovered when working on the project
	- `qubes-core-admin`: [API call implementation](https://github.com/QubesOS/qubes-core-admin/pull/359)
	- `qubes-core-admin-client`: [Client-side API](https://github.com/QubesOS/qubes-core-admin-client/pull/149)
	- `qubes-doc`: [Documentation](https://github.com/QubesOS/qubes-doc/pull/1027)

[qubes-repo-templates]: https://github.com/WillyPillow/qubes-repo-templates/
[#5946]: https://github.com/QubesOS/qubes-issues/issues/5946

## Mailing List Discussions ##

- [Introduction](https://groups.google.com/g/qubes-devel/c/AiGB5Qrl0lU/m/BFCOk83BBgAJ)
- [Progress and Plans](https://groups.google.com/g/qubes-devel/c/kHQkDIsHU3M/m/2M1vwPIsCAAJ)
- [Draft Design](https://groups.google.com/g/qubes-devel/c/6Zb_WLy3GY4/m/hAAniPXbBQAJ)
- [Installation Mechanism PoC](https://groups.google.com/g/qubes-devel/c/PyJogqT1TUg/m/hfNxCbQaCQAJ)
- [Repo Interaction](https://groups.google.com/g/qubes-devel/c/2XaMP4Us3kg/m/5U_0fca8BwAJ)
- Next Steps (incl. GUI, docs, tests, etc.): [1](https://groups.google.com/g/qubes-devel/c/wF_84b1BR0A/m/_z5QT685DAAJ) [2](https://groups.google.com/g/qubes-devel/c/pYHnihVCBM0/m/HDehThBbAAAJ)

## Progress ##

During the initial discussion, it was determined that the GUI could be shifted later in favor of other features. As such, the component was delivered at a later stage compared to the initial proposal.

As of the end of August 2020, the project is in working shape, including the CLI and GUI tool. The main aspects remaining are the following:

- Proper packaging of some scripts
- Comprehensive test cases
- Integration with the current system, e.g., Anaconda installer, salt management stack
- Possible integration/merge with existing template manager (used for setting templates of AppVMs)

## Acknowledgements ##

Huge thanks to my mentors [Marek Marczykowski-Górecki][marekek] and [Wojtek Porczyk][woju] for their amazing assistance. Also, I greatly appreciate [Andrew David Wong][adw] and [Marta Marczykowska-Górecka][marmarta] for reviewing my pull requests. Last but foremost, I would like to thank the Qubes OS team for maintaining an awesome project and Google for giving me the opportunity to work on this proposal with such wonderful people.

[marekek]: https://www.qubes-os.org/team/#marek-marczykowski-g%C3%B3recki
[woju]: https://www.qubes-os.org/team/#wojtek-porczyk
[adw]: https://www.qubes-os.org/team/#andrew-david-wong
[marmarta]: https://www.qubes-os.org/team/#marta-marczykowska-g%C3%B3recka
