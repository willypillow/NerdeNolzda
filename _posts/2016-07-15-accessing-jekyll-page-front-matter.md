---
layout: post
title: Accessing Jekyll Page Front Matters
date: 2016-07-15 13:57:04 +0800
last_modified_at: 2021-02-25 17:06:51 +0800
tags: en jekyll
---

As you might have noticed, I've added dates on pages like [About]({{ site.baseurl }}/about/), mainly because of [schemas](https://schema.org/). However, even when `date` is added to the front matter, the following snippet in `_layouts/page.html` does not work at all:

``` html
{% raw %}
<time datetime="{{ page.date | date_to_xmlschema }}" itemprop="datePublished">{{ page.date | date: "%b %-d, %Y" }}</time>
{% endraw %}
```

If the above is used, Jekyll shows the following error message:

```
Invalid Date: '' is not a valid datetime.
Liquid Exception: exit in _layouts/page.html
```

However, using `{% raw %}{{ page.date }}{% endraw %}` directly displays the information as expected. After a lot of trial and error, I found a workaround:

``` html
{% raw %}
{% capture pubDate %}{{ page.date }}{% endcapture %}
<time datetime="{{ pubDate | date_to_xmlschema }}" itemprop="datePublished">{{ pubdate | date: "%b %-d, %Y" }}</time>
{% endraw %}
```

The following also works:

``` html
{% raw %}
<time datetime="{{ page['date'] | date_to_xmlschema }}" itemprop="datePublished">{{ page['date'] | date: "%b %-d, %Y" }}</time>
{% endraw %}
```

This might be related to how the following doesn't work in Jekyll plugins:

``` ruby
Jekyll::Hooks.register :pages, :post_write do |document|
  print document.date
end
```

While the following works fine:

``` ruby
Jekyll::Hooks.register :pages, :post_write do |document|
  print document['date']
end
```

However, I do not know why this is the case. After all, this seems a bit unintuitive, and does not seem to be well-documented.

