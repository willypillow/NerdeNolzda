---
layout: post
title: Revoking My GPG Keys
date: 2017-10-23 23:10:00 +0800
last_modified_at: 2021-02-28 03:09:26 +0800
tags: en gpg encryption security
---

	-----BEGIN PGP SIGNED MESSAGE-----
	Hash: SHA256
	
	Unfortunately, it seemed that I remembered the passphrase to my GPG master key incorrectly, effectively losing access to it. As such, I have no option but to revoke my old keys (luckily, I had the revocation certificate backed up) and create a new one.
	
	The revocation certificate is as follows:
	
	```
	- -----BEGIN PGP PUBLIC KEY BLOCK-----
	Comment: A revocation certificate should follow
	
	iQQfBCABCgAJBQJYhE6mAh0AAAoJEOtNMmSjGHPLnWkf/jIz44PZefbBrTGHmCDO
	eGxd/g6nKASjc4TClK69OpQPswR94Lbl1Ia+bV67VPbf05xMXSEViFSOHvgqOjv1
	GJ7vybh3hE/9l8dH3WkellaR/yR9eCknqFl/e48z+P+bnbJ9CG7uw0rxFHvFO6/I
	4/4etj1pIpAGXe6mw2t+HIomOuu9zlp4KcKkE0dqfrpF8wn+E+n6U298nnW51EBB
	RInfQrED726+jc1uROTsUqCbfMF0NBpJwYXRHbpQtR+SZXXVU+JLrF4fpxPqhC5S
	XocFGXB8bK2ELA97CUzJflUaGV4P2KeuvG6kaNPG8gd6ep6KVKQClS8l3NTvtb0p
	pUKW0VdO7CMrw3GiqUoRZ5KpfVnbQHbHVVlel7iIJizFS2uJ2xMwPx0YNnVxWE26
	UHeQgJiwhy9KvlU0byi2zV2BCAX8ezJBG/noa3+nmZknclv/CI4QVjXf0fw02xch
	vrYRYvc5S9cnbNp2+4iwHS4i3V3nH4nN3EpwVsaHQ6giK0nM5GOlKLJG1ohM0Vy4
	iSukzwCcHgrkEsE0ybrnQrBSZ1w/lilemwSIWYYmqCNsZ2ied6vL6G0dUVXz+ipZ
	yw5M22tIMFcSPfBZJir5otf6e7WSygm75QRH54yKHedXD2fKiFcdSi+jA2lF66io
	OVe8dIFrS6om6NNjEoxr7Ejg5eA2j/yxHEEKGKFGSBnuQMvKTnHAXrM/JwtSO5pq
	naSpiURbSQlgMJRUmpFtjOTodJ3bqT7kHo5c7Y2L3HdqS4Biiqukx63PQqd3UBdb
	h//UiukPu+njDiJf1wsbw4BJIwJDa3SD/ceXiikP2gLKTX6UR9HfDpokNiAnmm+o
	GR8+H24OB8XEFwCKYu7QQ5NyykJa2+KukBwEkt22hk+srjlfmlOIsD78+x3V3QGn
	uLj3HfiHpuKc0djJnDroG6uppi3G2sVG4UU4UOEcD2L9RtAL8w3km+iPJe9hnHrZ
	ifk35RzDjJmezzzx4+IqV6fAVTj+KYTZUN/uKzCgT+Y8XwqXrvmxXOy7CGji5zKK
	FDLU1jrN3f/rrPounoBtzc6oNUK5pJ0Zpvtb2f4kh0jjMvHA+v6zZ48M7DC0qUHB
	tA7g/G7+pETfb93JXrXCL0rzK2bV7aahp9ylZI2BlPmU2Cw+/VfQx70eeQEcF5hu
	K17++wLlbWweZwqX+vvP0ImtoKEj8UkbNzJUSOtqNd94VMsTurZVeXzSAkHtLS8E
	66Rv76Nu1EtTrARr2IcnpORuktc/zr9xIhuH5RfqJbDnEE7Gfv0+qdsOwhKHvE42
	zyu3eALTUR5JXSiLxQvn6snTVr79M2RF0RS4RGj9DsB75OHZXVjHaeg86/dvzZpK
	hzw=
	=D2/M
	- -----END PGP PUBLIC KEY BLOCK-----
	```
	
	The new public keys are as follows:
	
	```
	pub   4096R/E7C31C84 2017-10-23
	      Key fingerprint = 6CCF 3FC7 32AC 9D83 D154  217F 1C16 C70E E7C3 1C84
	uid       [ultimate] WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
	sig 3    N   E7C31C84 2017-10-23  WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
	   Signature notation: comment_en@openpgp-notations.org=Master Signing Key
	sub   4096R/B1415A9C 2017-10-23
	      Key fingerprint = 2EF5 D56D 9574 A58B A2A8  F60C 3839 E194 B141 5A9C
	sig      N   E7C31C84 2017-10-23  WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
	   Signature notation: comment_en@openpgp-notations.org=Code Signing Key
	sub   4096R/C56F2FE7 2017-10-23 [expires: 2018-10-23]
	      Key fingerprint = 504E 5836 D13E 824D C4D6  4621 C12A E5AC C56F 2FE7
	sig      N   E7C31C84 2017-10-23  WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
	   Signature notation: comment_en@openpgp-notations.org=Email Signing Key
	sub   4096R/F72D8561 2017-10-23 [expires: 2018-10-23]
	      Key fingerprint = 549D 6337 A26D D92A 1197  5613 4C66 C9AE F72D 8561
	sig      N   E7C31C84 2017-10-23  WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
	   Signature notation: comment_en@openpgp-notations.org=Email Encryption Key
	```
	
	Note that the master key is 4096-bit instead of 8192. This is because I am convinced that [the latter is not significantly stronger than the former](https://www.reddit.com/r/GnuPG/comments/3h36qz/why_no_large_8192_16384_bit_pgp_keys_in_practice/) and is not worth the trouble (the generation process being more complex) and performance hit.
	
	The subkeys are now tagged via notations to indicate their usage, and can be viewed by `gpg --list-sigs --list-options=show-notation`.
	
	In addition, the master key is generated and used on a Qubes OS vault instead of the air gap I used before due to the following reasons:
	
	* The air gap is an old laptop that is extremely sluggish and annoying to use
	* As it is quite aged, I am concerned that it might die suddenly
	* Transferring data from it to the main box is quite difficult (currently using Data Matrix codes)
	* The wireless chips might pose security risks (no BIOS options to disable; difficult to find removal instructions online due to its age; do not have confidence in my hardware-hacking skills)
	* Need additional passphrases (for either FDE or the key), which are prone to being forgotten
	
	I will soon update my profiles online to reflect this change.
	
	This post, aside from being signed with my Git subkey, is also being signed explicitly by my email key as additional evidence to prove my identity.
	-----BEGIN PGP SIGNATURE-----
	Version: GnuPG v2
	
	iQIcBAEBCAAGBQJZ7gTuAAoJEKQZmDSlCA9O0z8QAK5aMfC81jXdUB1mUvXyA1kG
	1Dr0TOBSS6K04p2fdxduT5uCoVz4ATkt5FFzpIuYHTZKz5mLpEIRq3ogDijdiQ9P
	lu5a42gB0Quf0eh6VMT0SHrsHBfoCN7NHF66SuivGtTe8Kq4mq60C7uAIfQyzxJC
	kaZ69t/NRrGzVuivqQbh9Rmc1AVT6RC5M7VfYEC87YY3oNcOjtNBRcDzn/vn3O81
	ESGQosnT9T+KixWJYKXX8jTRTxeAm0Yd/r9A88in0k/t3GcINo7swKsclXKVgDH0
	6FPlmHjzkPHHszyXzvtU1gB4rt482yMQvANg5abBqqFBZ5nq8/CKksLmU5WyoUfz
	Rn+o8zPi5ebq664gIcKdQhvqyLobpHg12Jccq9qo9wuRrbPTqTiN1Mh9SZF/8Ef0
	gFzUDYr1Rv2kcCYgqb6gJGvDLwkwUNrmtcMSNzTupOwTxsd/bXWzBr5jAQ55jf8X
	I1ZpcYN+B+kGVqdXkCvTZkHy9stm/f+qbXed72U+BxXFzaLKlOywI2ZFxwE4zvnl
	Lgb6eITYfTEgJMAJ58j8JTjG0OWvpfPEkw+fzX9amUWLWhnzDe52lRH7f2Aw/lQg
	FskaI2M44dbpkwYvOY81Xt833lCfEsDwUZ7BzvhtdK0Rn/TCy2KAC3ksTL+Ckl5m
	QPjAkVfe6TkT6hQCY3uh
	=tL8r
	-----END PGP SIGNATURE-----
