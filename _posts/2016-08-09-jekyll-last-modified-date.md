---
layout: post
title: "Jekyll Last Modified Date"
date: 2016-08-09 18:43:26 +0800
last_modified_at: 2018-06-02 22:24:59 +0800
tags: en jekyll git
---

I've installed [Arch Linux](https://www.archlinux.org/) onto a USB drive, essentially creating a portable Linux installation. However, once I cloned the repo of this blog, I found out that the modification times of the files (obviously) changed. Since these were used as the post modifications dates, they were messed up. Thus, I needed to find a different way of tracking the modification dates on the posts.

The module [jekyll-last-modified-at](https://github.com/gjtorikian/jekyll-last-modified-at) seemed to be promising, but unfortunately it havn't been updated in a while, and conflicts with another plugin that I use on Cygwin.

Eventually I came across [this StackOverflow post](http://stackoverflow.com/a/33721446), which seemed to be a great idea. After a bit of fiddling, it worked like a charm.

For the full changes that I did, see the related commits [this](https://bitbucket.org/WillyPillow/nerdenolzda/commits/86ad6f3277d7e3a1230c9822d74fd52e695d825f) and [this](https://bitbucket.org/WillyPillow/nerdenolzda/commits/e383be5e39f65dc3be6daf107056c9e755e89caa).

One thing worth noting is that the ```mv``` in the script seemed to mess up the permissions of the deploy script, and I figured the easiest way to get out of it is to use ```sed -i``` instead, which edits the files in-place. One thing to keep in mind, however, is that this isn't a POSIX standard, making it less portable.

The source of the pre-commit hook is posted below, since git didn't seem to track it:

``` sh
#!/bin/sh

echo Running moddate...
git diff --cached --name-status | while read a b; do
  sed -i "/---.*/,/---.*/s/^moddate:.*$/moddate: $(date "+%Y-%m-%d %T %z")/" $b
  git add $b
done
```

