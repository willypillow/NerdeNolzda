---
layout: post
title: Jekyll Site Optimization
date: 2017-01-27 19:58:00 +0800
last_modified_at: 2021-02-25 17:07:07 +0800
tags: en site jekyll
---

I used to use [Gulp](https://gitlab.com/willypillow/JekyllGulp) for optimizing the pages on the site for speed. However, because I moved to Netlify for hosting and building the site, I didn't want to use something that not only required quite a few dependencies but was also rather slow. In the article below, I will list the tools I am using as alternatives right now.

CSS Minify
---
This is actually pretty easy, since it turns out that `sass`, which Jekyll use to generate CSS files, has an option that does exactly this. Just add the following to `_config.yml`:
```
sass:
  style: compressed
```

HTML Minify
---
[This layout](https://github.com/penibelst/jekyll-compress-html) does the job nicely. Basically, just throw `compress.html` in `_layouts`, set the layout of `default.html` to `compress`, and optionally tweak the settings in `_config.yml`.

A special thing about this is that it was completely written in Liquid, without requiring any extra plugins. Thus, it can also work with Github Pages, where the automatic building of Jekyll sites does not support plugins.

CSS Inline
---
As written [on Google](https://developers.google.com/speed/docs/insights/InlineCSS) and a lot of other websites, it is generally recommended to inline (small enough) CSS files in HTML documents since it eliminates an additional request that potentially blocks the display of content.

A lot of articles I've read recommended moving `main.scss` to `_includes/` and replacing `<link rel="stylesheet"...` line in `head.html` with the following code:

{% raw %}
```
{% capture scss %}{% include main.scss %}{% endcapture %}
{{ scss | scssify }}
```
{% endraw %}

However, I realized that `scssify` is called for each post, resulting in a lot of redundant computations. Therefore, I came up with [a simple Ruby plugin](https://gitlab.com/willypillow/NerdeNolzda/blob/master/_plugins/inline_css.rb) that essentially replaces the CSS import line in the generated HTML files with the CSS content itself. This way, the CSS is only generated once, drastically improving the speed (about a 60% speedup).

Removing Redundant CSS
---
`UnCSS` was used to remove unused or redundant CSS rules in the Gulp script. However, there doesn't seem to be an Jekyll/Ruby-only equivalent. Thus, I just manually compared the raw styles and the ones processed by UnCSS, commenting out the rules I didn't use. Admittedly, this isn't as complete as the previous solution, since my Gulp script optimizes the CSS for each HTML file separately, but I guess it works well enough.

Not Implemented
---
The following are the functionalities in the script that I deemed unnecessary, thus did not search for alternatives:

### Optimize Images
There aren't many images on this site to begin with, and I could always manually compress them if needed.

### Optimize XML
Again, there aren't a lot of XML files on this site, and most of them are usually processed by servers that aren't that sensitive about bandwidth anyway.
