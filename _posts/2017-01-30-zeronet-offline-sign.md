---
layout: post
title: Zeronet Offline Signing
date: 2017-01-30 10:36:00 +0800
last_modified_at: 2021-02-25 17:07:09 +0800
tags: en encryption security qubes zeronet
---

As can be seen [here]({{ site.baseurl }}/mirrors/), this site now has a [Zeronet](https://zeronet.io/) mirror. In the process of creating the mirror, I figured it would be nice to sign the site (`content.json`) on a separate vault VM in [Qubes OS](https://www.qubes-os.org/) so that the chances of the private key leaking is minimalized. However, there did not seem to be any articles about achieving such a thing at the time. As such, I will document my method in the following article.

1. Create a (vault) VM and install Zeronet on it
The official insturctions are [here](https://github.com/HelloZeroNet/ZeroNet#manual-install-for-debian-linux). It is recommended, however, that you use `git clone https://github.com/HelloZeroNet/ZeroNet.git` to clone the repo and verify the signatures before running.
To verify the signature of the repo:
```
cd ZeroNet/
# Fetch the keys, the hex string is the public key at the time of writing
gpg --recv-keys 960FFF2D6C145AA613E8491B5B63BAE6CB9613AE
# Get latest commit
COMMIT=`git log --oneline | head -n 1 | cut -f 1 -d ' '`
# Verify the signature
git verify-commit "$COMMIT"
# If the command above outputs "Good signature", checkout the commit
git checkout $COMMIT
```
2. Cut off Internet access to the VM (i.e. set firewallVm to none)
3. Create a key pair
```
# Remember to save the private key somewhere safe
./zeronet.py siteCreate
```
4. Modify the site
Copy the files to `data/[YOUR-PUBLIC-KEY]`.
5. Sign the site
```
# This will ask for your private key
./zeronet.py siteSign [YOUR-PUBLIC-KEY]
```
6. Install Zeronet on another VM (You may have to have your port opened. [Link](https://zeronet.readthedocs.io/en/latest/faq/#do-i-need-to-have-a-port-opened) and [link](https://www.qubes-os.org/doc/firewall/))
7. Copy your site directory (`data/[YOUR-PUBLIC-KEY]`) from the vault to the same directory on the Internet-connected VM
8. Publish the site
```
./zeronet.py sitePublish [YOUR-PUBLIC-KEY]
```

And you're finished. In theory, it should be possible to use [qrexec](https://www.qubes-os.org/doc/qrexec3/) and create something similar to [split GPG](https://www.qubes-os.org/doc/split-gpg/) in order to make the process simpler, and is definitely something I should look into.
