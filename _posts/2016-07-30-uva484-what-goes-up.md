---
layout: post
title: "UVa484: What Goes Up"
date: 2016-07-30 09:28:48 +0800
last_modified_at: 2018-08-22 20:59:34 +0800
tags: en zh bilingual UVa cpp competitive-coding lis patience-sorting
---

實作頗標準的 Patience Sorting。
Imeplement standard patience sorting.
[Patience Sorting - Wikipedia (En)](https://en.wikipedia.org/wiki/Patience_sorting)
[演算法筆記 - Longest Increasing Subsequence (Zh)](http://www.csie.ntnu.edu.tw/~u91029/LongestIncreasingSubsequence.html)

[UVa Link](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=728)

``` cpp
#include <cstdio>
#include <algorithm>
using namespace std;

const int INF = 1000000, MAX_SIZE = 1000;

int main() {
  int size = 0, tmpIn, lastIndice = 0;
  int in[MAX_SIZE], tmp[MAX_SIZE], pos[MAX_SIZE] = { 0 };
  fill(tmp, tmp + MAX_SIZE, INF);

  while (scanf("%d", &tmpIn) == 1) in[size++] = tmpIn; // End output with ^D

  for (int i = 0; i < size; i++) {
    int *insertPtr = lower_bound(tmp, tmp + size, in[i]);
    *insertPtr = in[i];
    pos[i] = insertPtr - tmp; // Save inserted position
    if (pos[i] > lastIndice) lastIndice = pos[i];
  }

  printf("%d\n-\n", lastIndice + 1);

  int posCount = lastIndice;
  for (int i = size - 1; i >= 0; i--) {
    if (pos[i] == posCount) {
      tmp[posCount--] = in[i]; // Reuse tmp, heh
    }
  }

  for (int i = 0; i < lastIndice + 1; i++) printf("%d ", tmp[i]);
  return 0;
}
```
