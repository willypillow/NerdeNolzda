---
layout: post
title: Simulating Middle Clicks on Windows Touchpads
date: 2016-07-17 08:26:42 +0800
last_modified_at: 2021-02-25 17:06:57 +0800
tags: en windows t100 touchpad autohotkey
---

As mentioned before, I have a Asus T100 stuck on Windows 10 due to touchpad multitouch compatibilty issues with Linux. However, even on Windows, the default driver [Asus Smart Gestures](https://www.asus.com/support/faq/1009613) does not have the option to send middle clicks. This becomes an annoyance when browsing via Firefox. Thus, I decided to use [AutoHotkey](https://autohotkey.com/) to do the trick.

The first thing I tried is a solution that can be easily found on the Internet:

```
~LButton & RButton::
MouseClick, Middle
Click up left
return

~RButton & LButton::
MouseClick, Middle
Click up right
return
```

It send a middle click if you left click and right click at the same time. However, I quickly found out that the T100 touchpad can not be used this way. Due to its mechanical design, if you hold down the left button, there will be no way to press the right button, and vice versa.

Anyway, I turned to the three-finger-click action. By default, it launches the search dialog in the start menu. After a bit of investigation, I realized that a `Win+S` is sent whenever the action is performed. So I came with the following one-liner:

```
#s::MouseClick, Middle
```

And sure enough it worked. Of course you lose the functions of `Win+S`, but I don't use that, and would trade it for middle clicking anyday.

