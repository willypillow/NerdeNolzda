---
layout: post
title: "UVa299: Train Swapping"
date: 2016-08-09 18:40:20 +0800
last_modified_at: 2018-06-02 22:25:02 +0800
tags: en zh bilingual cpp UVa competitive-coding
---

實作泡沫排序，並紀錄次數。
Implement bubble sorting and log the swap count.

[UVa Link](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=235)

``` cpp
#include <iostream>
#include <cstdio>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  int t;
  std::cin >> t;
  while (t--) {
    int n, arr[100], swap = 0;
    std::cin >> n;
    for (int i = 0; i < n; i++) {
      std::cin >> arr[i];
    }
    for (int i = 0; i < n; i++) {
      int jTimes = n - i - 1;
      for (int j = 0; j < jTimes; j++) {
        if (arr[j] > arr[j + 1]) {
          int tmp = arr[j + 1];
          arr[j + 1] = arr[j];
          arr[j] = tmp;
          swap++;
        }
      }
    }
    std::cout << "Optimal train swapping takes " << swap << " swaps.\n";
  }
}
```

