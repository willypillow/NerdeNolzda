---
layout: post
title: Jekyll GPG Signatures
date: 2017-01-27 08:27:00 +0800
last_modified_at: 2021-02-25 17:07:05 +0800
tags: en site gpg encryption security
---

As mentioned [here](/keys/), the Git repo of this blog is PGP signed. However, I realized that it isn't exactly convenient to verify the signatures since you have to clone the whole repo. Thus, I started searching for a way to serve the signatures along with the HTML.

Most of the solutions I've found (e.g. [jekyll-gpg_clearsign](https://github.com/kormoc/jekyll-gpg_clearsign) and [another one that only signs the Markdown files](http://blog.merovius.de/2014/01/23/signed-blog-posts.html)) all require the private key in the building process, which is not optimal for me since this site is built on a third party server (i.e. Netlify). In addition, they both embed the signature directly in the HTML, which kind of wastes bandwidth for the majority of users who don't care about verifying.

Therefore, I decided to roll my own solution. In a nutshell, the signed Markdown files are placed in a different directory, which are then linked to from the article (i.e. the `Signature` link just below the title).

Implementing it is fairly simple. Basically, it boils down to writing a pre-commit hook that signs changed files (much like [my modified date mechanism]({% post_url 2016-08-09-jekyll-last-modified-date %})) and editing the page layout to include the link.

For the pre-commit hook, see [this commit](https://gitlab.com/willypillow/NerdeNolzda/commit/8c3238af192cdd3bfc7158a62d38d2d75c59b18e) (lines 8~15). As for the layout, see [this commit](https://gitlab.com/willypillow/NerdeNolzda/commit/258907261be144444a6cb7263e8c5deb822de62e) (`_layouts/page.html` and `_layouts/post.html`).

As you can see from the hook, only `*.md` files are signed. This is due to the fact that those files are mainly posts, while other files (e.g. `*.html`) are mainly layouts, config, ..etc.

One interesting thing is that Jekyll seems to treat files under `*/_posts/` as posts rather than plain static files. Thus, I had to jump through a few hoops, replacing `_posts` with `posts` in the path of the signature file, and modify the link to it accordingly.

Verifying
---
To verify the signature, just download it and pass it to `gpg`. For example, for this page, you can run the following:
```
curl {{ site.url }}{{ site.baseurl }}/sigs/posts/2017-01-27-jekyll-gpg-signature.md.asc | gpg
```
which outputs the Markdown content and whether it is properly signed or not.

It is important to note that, since the HTML is not signed, an attacker might modify it, making it different from the Markdown file. As a result, even if the Markdown file's signature is valid, it does NOT mean that you can trust the web page. Instead, only the Markdown content that is outputed by GPG is guranteed not to be modified.
