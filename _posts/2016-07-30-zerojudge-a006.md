---
layout: post
title: "ZeroJudge: a006 一元二次方程式"
date: 2016-07-30 10:00:13 +0800
last_modified_at: 2018-06-02 22:24:41 +0800
tags: en zh bilingual zerojudge c competitive-coding
---

公式解。
Solve by the formula.

[ZeroJudge Link (Zh)](http://zerojudge.tw/ShowProblem?problemid=a006)

``` c
#include <stdio.h>
#include <math.h>

main() {
  int a, b, c;
  while (scanf("%d %d %d", &a, &b, &c) != EOF) {
    if (b * b - (4 * a * c) > 0) printf("Two different roots x1=%d , x2=%d\n", (int)((-b+sqrt((double)b*b-4*a*c))/(a*2)), (int)(-b-sqrt(b*b-4*a*c))/(a*2));
    else if (b * b - (4 * a * c) == 0) printf("Two same roots x=%d\n", -b/(a*2));
    else printf("No real root\n");
  }
  return 0;
}
```

