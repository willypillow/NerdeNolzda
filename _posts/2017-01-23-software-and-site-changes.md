---
layout: post
title: "Software And Site Changes"
date: 2017-01-23 12:55:00 +0800
last_modified_at: 2021-02-28 01:49:52 +0800
tags: en site software privacy protonmail duckduckgo startpage searx omninotes gitlab netlify dnscrypt gpg encryption security
---

In the previous few week, there were quite a few changes to the software and services that I use. As a consequence, the services that this site relys on also changed a bit.

Gmail + Zoho Mail -> Protonmail
---
The first thing is that I've decided to ditch Google services and move to more privacy-respecting providers. As of now, the only Google services that I use are Youtube, Maps, and Play. Anyway, I switched to [Protonmail](https://protonmail.com/) mainly because of its good reputation, and the ability to accept Bitcoins and use custom domains.

I've been pretty satisfied with it, the UI/UX along with the mobile app is pretty decent. The storage, even on premium, is a tad smaller compared to Gmail (5 GB versus 15 GB), but it's more than enough for me. A downside, however, is that it doesn't allow you to export your mail, but I guess I don't really have that much mail that needed to be kept for a long time. Besides, it'll be solved once the IMAP bridge, which allows you to use ordinary IMAP clients to access the service,.comes out (currently in closed beta).

Being able to use custom domains also means that instead of using Zoho Mail for my \*@nerde.pw addresses and setting up POP3/SMTP in my Gmail account, I can just use the address as my main account. However, since Protonmail doesn't currently support IMAP, and there are no separation of accounts (i.e. all mail to \*@nerde.pw gets sent to the same inbox), the automatic comment system can not be used. (But again, although the code is already there, the daemon has never been started in production due to my laziness XD)

BTW, Some might say that Javascript crypto (i.e. what Protonmail is using) is insecure, and you still have to trust the server not feeding you bad code. I agree with that, but again most of the mail I receive comes from different providers, so I have to trust Protonmail anyway. In addition, for more sensitive communication, it would be dumb not to encrypt them with [my own PGP keys]({{ site.baseurl }}/keys/).

Google -> Duckduckgo -> Startpage -> Searx
---
As a part of my plan of ditching Google, I tried a lot of search providers.

[Duckduckgo](https://duckduckgo.com/) is the first one that I hopped to. It's probably the most famous one of the kind, being built-in in a lot of browsers. The UI and UX is great, and I personally even prefer it over Google.

However, after a few weeks I decided to change to [Startpage](https://www.startpage.com/) due to its web proxy service and the fact that Duckduckgo is US based. I have to say that the UI looks pretty outdated, and does not have a lot of fancy features that Duckduckgo provides, like showing Wikipedia previews at the top of the page.

Shortly after, I came across [Searx](https://github.com/asciimoo/searx/wiki/Searx-instances), which is open source, and, as a result, there are quite a few independent instances (you can even host your own). The UI is also great, with an useful sidebar with Wikipedia results, suggestions, and the like on the right. Another cool feature is that you can select which search engines it uses to serve your results, unlike Duckduckgo only using Bing and Startpage using Google. Though it doesn't provide a proxy service like Startpage, it shows a link to <https://web.archive.org> right next to the result, which is almost just as good, and can also serve as a backup when the site is down.

Google Keep -> Omni Notes
---
I used to use Google Keep for note taking on my phone, but I realized that I don't really have the need of syncing the data across devices. As such, I switched to a open source offline app, [Omni Notes](https://github.com/federicoiosue/Omni-Notes). It has pretty much all the features of Google Keep (except syncing and phoning home, of course). As of now, the only things that I feel could be added are a pinning feature that lets you pin important notes at the top, and Markdown support.

Bitbucket -> Gitlab
---
For my code, I've decided to move to [Gitlab](https://gitlab.com/) since it provides a open source community version (CE) that I can self-host when necessary. The UI, in my opinion, is also slightly prettier than Bitbucket.

My Gitlab profile can be found at the footer of this blog.

Github Pages + Cloudflare -> Gitlab Pages
---
~~Since I've moved to Gitlab for my code repos, I figured it would be nice to move my blog repo there too. Great things that [Gitlab Pages](https://pages.gitlab.io/) provide are the automatic building of static page generators via Gitlab CI and the ability to use custom TLS certificates, allowing you to use TLS for custom domains. This is unlike Github Pages, where I had to use Cloudflare to enable TLS, which might be considered bad for privacy for some.~~

However, [I've been running into some problems with the service](https://forum.gitlab.com/t/gitlab-pages-404-for-even-the-simplest-setup/5870) at the time I'm writing this article. Hopefully they'll fix the issue soon.

EDIT: Due to the issues, I have switched to [Netlify](https://www.netlify.com/), which provides the same, if not more, features as Gitlab. One nice thing that they have is the function to automatically set up [Let's Encrypt](https://letsencrypt.org/) TLS certificates for you, compared to the manual ([though documented](https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/)) setup on Gitlab. In addition, the content is served from their CDN, unlike the (AFAIK) single server of Gitlab Pages. Last but not least, they have compression enabled, allowing me to get a [100/100](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fblog.nerde.pw) on Google Page Insights.

However, since I stopped using Cloudflare and changed back to Namecheap for managing DNS, the domain of this site had to be changed a bit. See [this post]() for more details.

Signal
---
Admittedly, this is not so much of a switch, since I'm still using Line and Facebook Messenger (web only) with some people. Regardless, I've succeeded in convincing a couple of friends to start using [Signal](https://whispersystems.org/), an encrypted and open source chat app.

For the UX of the app, it's way better than Line, with no ads, significantly faster startup, and, most importantly, a feature that silents a certain contact's notifications for a period of time (e.g. 1 hour). This is great since I always disable notifications on Line, then forget to turn them back on.

Of course, there are some problems with Signal, such as [the use of Google services allow Google to potentially compromise your messages](https://blogs.fsfe.org/larma/2017/signal-backdoors/), [the lack of federation](https://lwn.net/Articles/687294/) (i.e. unlike protocols like XMPP, you can't host your own server and have it communicate with other servers), and the need of providing your cell phone number to register. However, for me these are not that problematic.

First of all, my phone is already riddled with tons of proprietary apps from Google, LG, ...etc that can't be removed, so my trust in my phone is already low anyway, and Google services or not doesn't make that much of a difference.

As for federation, it's also a great feature to have, but the lack of it isn't really a deal breaker. For one thing, changing chat apps might be considered easier than changing, for example, social media that has more of your data on it. Also, it's arguable that federation is slightly less user-friendly, raising the barrier of entry for non tech-savvy people. Last but not least, the claim that federation slows down improvement is pretty reasonable.

Lastly, the need of a phone number doesn't bother me a lot, either. After all, I'm not using the app for anonymity. (Even if you were, there are services that receive verification SMSs for you)

Diaspora\*
---
Like Signal, this is more of a new service rather than a complete switch. Basically [Diaspora\*](https://diasporafoundation.org/) is a open source and federated social media software. Currently, I mainly use it to post links I've read or stuff that's not long enough to be on this blog, while none of my friends in real life are using it.

However, the lack of a groups feature may be a block for users of Facebook, Google+, and the like.

My Diaspora\* profile can be found at the footer of this blog.

DNScrypt
---
I used to be using Google and OpenNIC DNSes in plain text. Recently, however, I installed dnscrypt-proxy on a Qubes OS VM and started using DNScrypt, which allows you to encrypt your DNS queries. The whole thing is pretty invisible to the user, so there's not much about to say about this.

FYI, the server I'm using right now is `ns0.dnscrypt.is`.

Gnupg
---
As can be seen [here]({{ site.baseurl }}/keys/), I've started using [GPG](https://www.gnupg.org/) to sign stuff. More details about my setup will be covered in a future post.

Conclusion
---
To be honest, I'm still thinking about whether I should self-host some services or not. On one hand, it sure gives me more control. On the other hand, getting a VPS costs money, and hosting at home kind of brings forth security risks. That being said, I might get a dirt cheap VPS and experiment with it before I make a decision.
