---
title: "Freenet on Qubes"
layout: post
date: 2017-02-06 18:15:00 +0800
last_modified_at: 2021-02-25 17:07:11 +0800
tags: en security qubes freenet
---

As a fan of [Freenet](https://freenetproject.org/) due to its decentralization and anonymity and an user of [Qubes OS](https://www.qubes-os.org/), I wanted to combine these two to achieve better security. Specifically, my goal is to make it work like [Whonix](https://freenetproject.org/), splitting it into a "Freenet Gateway" and "Freenet Workstation", with the latter not being able to connect to other places except the gateway.

Steps
---
1. Create a firewallVm as the gateway, with the Freenet software installed on it.
2. Create an appVm that uses the gateway as its firewallVm. This will act as the workstation.
3. Right click the workstation entry in the VM manager, click "VM Settings", and take note of the "IP" and "Gateway" address. These will be referred to as `[IP]` and `[GATEWAY]` respectively in the following steps.
	![VM Settings]({{ "/assets/freenet-on-qubes/1.png" | relative_url }})
4. Navigate to the "Firewall rules" tab in the workstation settings, choose "Deny network access except...", and untick all three boxes on the right.
	![Firewall rules]({{ "/assets/freenet-on-qubes/2.png" | relative_url }})
5. In the webUI of Freenet (on the gateway), navigate to "Configuration -> Web Interface"
	![Web Interface Config]({{ "/assets/freenet-on-qubes/3.png" | relative_url }})
6. Change "IP address to bind to" to `[GATEWAY]`
	![IP to Bind to]({{ "/assets/freenet-on-qubes/4.png" | relative_url }})
7. Change "Hostnames or IP addresses that are allowed to connect to the web interface" to `127.0.0.1,[IP],[GATEWAY]`.
8. Change "Hosts having a full access to the Freenet web interface" to `127.0.0.1,[GATEWAY]`.
	![Allowed Hosts]({{ "/assets/freenet-on-qubes/5.png" | relative_url }})
9. In the gateway, run the following commands:
	```
	echo 'iptables -I INPUT -s [GATEWAY0]/24 -d [GATEWAY] -p tcp --dport 8888 -j ACCEPT' | sudo tee -a /rw/config/qubes-firewall-user-script
	sudo chmod +x /rw/config/qubes-firewall-user-script
	```
	![Iptables]({{ "/assets/freenet-on-qubes/6.png" | relative_url }})
10. Reboot the gateway

This way, Freenet can be accessed in the workstation VM at `http://[GATEWAY]:8888/`.

Also, note that, due to how networking in Qubes works, you may have to start the workstation VM before launching Freenet on the gateway.
