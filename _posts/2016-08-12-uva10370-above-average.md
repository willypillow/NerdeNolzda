---
layout: post
title: "UVa10370: Above Average"
date: 2016-08-12 08:28:35 +0800
last_modified_at: 2018-06-02 22:25:04 +0800
tags: en zh bilingual cpp UVa competititve-coding
---

簡單地算出平均並迴圈計算大於平均的人數。
Simply calculate the average and loop to count the number of people having a score higher than that.

[UVa Link](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=1311)

``` cpp
#include <iostream>
#include <iomanip>

int main() {
  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);
  std::cout << std::fixed << std::setprecision(3);
  int c;
  std::cin >> c;
  while (c--) {
    int n, arr[1000], count = 0;
    float avg = 0;
    std::cin >> n;
    for (int i = 0; i < n; i++) {
      std::cin >> arr[i];
      avg += arr[i];
    }
    avg /= n;
    for (int i = 0; i < n; i++) {
      if (arr[i] > avg) count++;
    }
    std::cout << (float)count / n * 100 << "%\n";
  }
}
```

