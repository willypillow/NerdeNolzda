---
layout: post
title: Simple Monero Mining Botnet Analysis
date: 2016-07-12 21:55:48 +0800
last_modified_at: 2018-06-02 22:24:21 +0800
tags: en security malware botnet monero cryptocurrency
---

I was browsing [Code Scenic](http://codingsimplifylife.blogspot.tw/), a Chinese blog written by a friend the other day, when I discovered [this post](http://codingsimplifylife.blogspot.tw/2016/07/blog-post.html).

Simply put, he found his flash drive infected with a virus, which moved his data into a hidden folder (`_`) and created a shortcut in the root of the disk that will not only open the hidden directory, but also infect your box. Anyway, the reason I'm writing about this is that my family happened to be victims of the malware just a few months ago.

## Infection To Box (movemenoreg.vbs)
The shortcut mentioned above directly invokes `movemenoreg.vbs`, which moves these four files from `{DISK}:\WindowsServices` into `%APPDATA%\WindowsServices`:

* helper.vbs
* installer.vbs
* movemenoreg.vbs
* WindowsServices.exe

if they're not existent, and make them hidden. Lastly, it launches `helper.vbs` if it is not yet started.

## Core (helper.vbs)
In contrast to its name, `helper.vbs` can be said to be the main part of the malware, which does the following:

1. Add a shortcut to itself to the startup folder
1. `installer.vbs` is called to infect other USB devices if it is not yet running
1. `WindowsServices.exe`, a Monero mining client, is launched if it is not started

The three steps above are repeatedly executed every 5 seconds.

## Infection To USB (installer.vbs)
Every time the script is started, it scans for connected USB devices and enter a infinite loop where it then does the following:

1. Check the execution of `helper.vbs`
1. Create `\WindowsServices` if it is not existent
1. Copy the four files into the directory
1. Create the shortcut that launches `_` if it is not already there
1. Create `_` if it doesn't exist, and move all the files/directories except for `_`, `System Volume Information`, and `WindowsServces` into it

## Monero Mining Client (WindowsServices.exe)
This is just a normal Monero mining client, which can be easily downloaded from the Internet. In case you don't know what mining is, it's basically doing computations to verify transactions on the Monero network, which earns Monero coins (XMR), in this case, for the author of the botnet. For more information see [this Wikipedia article](https://en.wikipedia.org/wiki/Bitcoin#Mining).

You can get the wallet address and mining pool address from the arguments by which `WindowsServices.exe` is called. As for 2016-07-13, a search for the wallet address on the mining pool website shows that about 11.4 XMR (~23 USD) is made. Somehow, I feel sorry for the malware author. The program seemed to be on my family's computer since February or March, which means (s)he only earned that amount in at least 4 months.

## What Is Monero?
Basically, [Monero](https://getmonero.org/) is a cryptocurrency similar to Bitcoin. However, one big difference is that while Bitcoin is pseudoanonymous, Monero is completely anonymous.

For example, Alice has a Bitcoin wallet A. She sends the funds to Bob, Chris, and David, each with the wallet B, C, and D. While people peeking at these transactions can not simply link A, B, C, and D to their respective owners Alice, Bob, Chris, and David, they can trivially see that it's the same wallet A that sends money into B, C, and D.

On the other hand, in the case of Monero, there is simply no (simple) way to know that the same wallet A is used.

So why is this important? For the former case of Bitcoin, it's rather easy to link the wallet to the method of converting the funds into fiat currency, like buying and selling them at an exchange, and eventually trace to your personal identity. As for the latter case, since an attacker can't easily link the transactions done by the same wallet together, such methods become significantly more difficult.

Now, you might be complaining about how anonymous tools do no good except benefiting criminals. This is a complete fallacy. However, it's a bit off-topic to discuss this in this article. Hopefully, I'll cover the topic in another post in the near future.

## Conclusion
Even though this is only a set of simple VBS scripts, I feel that it's nice material for learning. The structure is quite easy, while having the ability to self-replicate. Also, the code is well-formatted, even with comments. (If you're a malware author writing in an interpreted language, be sure to obfuscate the code :D)

Lastly, CPU mining doesn't exactly make a lot of money.

Full code for the scripts can be found [here](https://bitbucket.org/snippets/WillyPillow/4j4p4).

