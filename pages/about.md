---
layout: page
title: About
date: 2016-07-04 20:23:37 +0800
last_modified_at: 2021-02-25 21:29:12 +0800
permalink: /about/
---

## What does "Nerde Nolzda" mean?

Roughly "Geek Palace" in [lojban](https://mw.lojban.org/papri/Lojban). Now, I know that I shouldn't be using capitalization like that in lojban, but it looks more consistent when used with English.

## What is this site about anyway?

In a nutshell, this is a blog mostly about computers, with a bit of other nerdy stuff. Me being somewhat of a generalist, the site covers a wide range of topics, so be sure to check out the [tags]({% link tags.html %}) page to see if there is anything you are interested in.

## Who is this "WillyPillow" guy?

I am an aspiring software developer who is interested in computer technology, Rubik's Cubes, anime, and other geeky stuff. I am currently located in Taiwan and able to speak Mandarin Chinese. Being a fan of [hacker culture](https://en.wikipedia.org/wiki/Hacker_culture), I believe in free software, lean libertarian/crypto-anarchist, and strive to promote freedom of information and personal privacy.

## Why the obscene handle?

Well, the handle was coined when I was pretty young. Not knowing the connotations, I intended for "Willy" to be short for "William," and the name stuck until now. A bit unfortunate[^1], but aside from the handle being banned from some game servers, most people are pretty understanding, so I see no reason to change it.

[^1]: Obviously NSFW, but if you do a web search, there are actually websites selling physical "willy pillows".
