---
layout: page
title: Keys
date: 2017-01-22 15:22:00 +0800
last_modified_at: 2020-02-16 16:23:08 +0800
permalink: /keys/
---

The following are my [GnuPG](https://gnupg.org/) keys for signing code and this blog, email encryption, and so on.
The keys can also be downloaded in the [keys/](https://gitlab.com/willypillow/NerdeNolzda/tree/master/keys) directory.

Private Key Security
---
The master key is handled in a clean [Tails](https://tails.boum.org/) environment, and the subkeys are loaded onto a [Yubikey](https://www.yubico.com/).

Master Key
---
The keys on this page are subkeys of the following master signing key.

```
pub   4096R/E7C31C84 2017-10-23
      Key fingerprint = 6CCF 3FC7 32AC 9D83 D154  217F 1C16 C70E E7C3 1C84
uid       [ultimate] WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
sig 3    N   E7C31C84 2017-10-23  WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
   Signature notation: comment_en@openpgp-notations.org=Master Signing Key
```

Note that you should verify this fingerprint through other channels, e.g. [my diaspora* profile](https://diasporing.ch/people/7abf8850bce50134011e7a163e59d8f4), email signatures on the Qubes OS mailing list ([qubes-users](https://groups.google.com/forum/#!forum/qubes-users) and [qubes-devel](https://groups.google.com/forum/#!forum/qubes-devel)), since it's possible that this repo was falsified.

Blog/Code Signing Key
---
The following key is used to sign this blog and other code repos.
```
sub   4096R/B1415A9C 2017-10-23
      Key fingerprint = 2EF5 D56D 9574 A58B A2A8  F60C 3839 E194 B141 5A9C
sig      N   E7C31C84 2017-10-23  WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
   Signature notation: comment_en@openpgp-notations.org=Code Signing Key
```
For the reason for the lack of an expiration date, see [Joanna Rutkowska's Blog](https://blog.invisiblethings.org/keys/) for more information.

Email Encryption Keys
---
The following keys were used for email communication. Note that they are not updated any more since I do not get that many emails. If you want to send me messages securely, just contact me and we can exchange [Signal](https://www.signal.org/) numbers (possibly signed). (I should probably get a secondary number for a public Signal account, but till then...)

```
sub   4096R/C56F2FE7 2017-10-23 [expires: 2018-10-23]
      Key fingerprint = 504E 5836 D13E 824D C4D6  4621 C12A E5AC C56F 2FE7
sig      N   E7C31C84 2017-10-23  WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
   Signature notation: comment_en@openpgp-notations.org=Email Signing Key
sub   4096R/F72D8561 2017-10-23 [expires: 2018-10-23]
      Key fingerprint = 549D 6337 A26D D92A 1197  5613 4C66 C9AE F72D 8561
sig      N   E7C31C84 2017-10-23  WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
   Signature notation: comment_en@openpgp-notations.org=Email Encryption Key
```

Old Keys
---
The following are old keys that were revoked. See [this post]({% post_url 2017-10-23-revoking-my-gpg-keys %}) for details.

> ### Master Key
> ```
> pub   rsa8192 2017-01-21 [C]
>       B57E7237B211419C35C4AF5BEB4D3264A31873CB
> uid           [ unknown] WillyPillow (https://blog.nerde.pw/) <wp@nerde.pw>
> ```
> 
> ### Blog/Code
> ```
> sub   4096R/B1E9DB2F 2017-01-21
>       Key fingerprint = 6DF4 7924 CDE6 6924 4E52  3286 F8B2 64E1 B1E9 DB2F
> ```
> 
> ### Email
> ```
> sub   4096R/A5080F4E 2017-01-22 [expires: 2018-01-22]
>       Key fingerprint = C775 4AF1 B865 CE73 315A  2936 A419 9834 A508 0F4E
> sub   4096R/0D7D2EFE 2017-01-22 [expires: 2018-01-22]
>       Key fingerprint = 5B8A 9A22 487B D9D8 264C  0EA3 904E 12B6 0D7D 2EFE
> ```
