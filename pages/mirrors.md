---
layout: page
title: Mirrors
date: 2017-01-29 15:44:00 +0800
last_modified_at: 2020-02-16 16:08:31 +0800
permalink: /mirrors/
---

The following are the locations that this site is currently hosted at:

Clearnet
---
* URL: <https://blog.nerde.pw/>
* Hosting: [Netlify](https://www.netlify.com/)
* TLS Certificate Issuer: [Let's Encrypt](https://letsencrypt.org/)

## Defunct

Due to the burden of maintenance, the Zeronet site is no longer updated.

> ### [Zeronet](https://zeronet.io/)
>
> * URL: <http://127.0.0.1:43110/1ncvmoP187AuEQkBsJHqEq8qcK1zcL2A9/>
> * URL (via Namecoin): <http://127.0.0.1:43110/blog.nerde.bit/>
> 
> Note that the Namecoin URL *might* be a little bit less secure than the longer one due to the fact that the Namecoin private key, unlike my PGP keys or Zeronet signing key, is not placed in a [Qubes OS](https://qubes-os.org/) network-less VM. That being said, as long as you verify the signatures ([keys]({% link pages/keys.md %}), [verifying guide]({% post_url 2017-01-27-jekyll-gpg-signature %})), which you absolutely should do, you should be fine.
